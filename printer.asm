.eqv Player.char 80
.eqv End.char 64
.eqv EndVictory.char 37

#this function get the argument of a CELL and then prints it
#modifies $t8
#needs to be called with $v0 set to 11
.macro printCell (%cell)
	li $t8, Cell.path.little
	beq %cell, $zero, printCell.path
	li $t8, Cell.wall.little
	beq %cell, $t8, printCell.wall
	li $t8, Cell.moved_path.little
	beq %cell, $t8, printCell.moved_path
	#li $t8, Cell.best_path.little 
	#beq %cell, $t8, printCell.best_path
	printCell.best_path:
		li $a0, Cell.best_path.char
		syscall
		b printCell.end
	printCell.moved_path:
		li $a0, Cell.moved_path.char
		syscall
		b printCell.end
	printCell.wall:
		li $a0, Cell.wall.char
		syscall
		b printCell.end
	printCell.path:
		li $a0, Cell.path.char
		syscall
		b printCell.end
	
	printCell.end:
.end_macro 

#this funtion prints every cell of a byte
#modifies $t9 temp
.macro printByte (%byte)
	#print 1st char
	andi $t9, %byte, Cell.mask.little
	printCell ($t9)

	#print 2nd char
	sll $t9, %byte, 2
	andi $t9, $t9, Cell.mask.little
	printCell ($t9)

	#print 3rd char
	sll $t9, %byte, 4
	andi $t9, $t9, Cell.mask.little
	printCell ($t9)

	#print 4th char
	sll $t9, %byte, 6
	andi $t9, $t9, Cell.mask.little
	printCell ($t9)

.end_macro 

#$t9 loopIndex
#$t6 temp
.macro printSpecialByte (%byte, %playerPos, %endLocation)
	li $at, 4 #end condition
	
	move $t9, $zero
	printSpecialByte.Loop:
		beq %playerPos, $t9, printSpecialByte.player
		beq %endLocation, $t9, printSpecialByte.endC
		andi $t6, %byte, Cell.mask.little
		printCell ($t6)
		sll %byte, %byte, 2
		addi $t9, $t9, 1
	bne $t9, $at, printSpecialByte.Loop
	b printSpecialByte.end
	
	printSpecialByte.player:
	li $a0, Player.char
	syscall
	sll %byte, %byte, 2
	addi $t9, $t9, 1
	bne $t9, $at, printSpecialByte.Loop
	b printSpecialByte.end

	printSpecialByte.endC:
	li $a0, End.char
	syscall
	sll %byte, %byte, 2
	addi $t9, $t9, 1
	bne $t9, $at, printSpecialByte.Loop

	printSpecialByte.end:
.end_macro 

.macro printPlayerByte (%byte, %playerPos)
	li $at, 4 #end condition
	
	move $t9, $zero
	printPlayerByte.Loop:
	beq %playerPos, $t9, printPlayerByte.player
	andi $t6, %byte, Cell.mask.little
	printCell ($t6)
	sll %byte, %byte, 2
	addi $t9, $t9, 1
	bne $t9, $at, printPlayerByte.Loop
	b printPlayerByte.end
	
	printPlayerByte.player:
	li $a0, Player.char
	syscall
	sll %byte, %byte, 2
	addi $t9, $t9, 1
	bne $t9, $at, printPlayerByte.Loop

	printPlayerByte.end:
.end_macro 

.macro printEndByte (%byte, %endLocation)
	li $at, 4 #end condition

	move $t9, $zero
	printEndByte.Loop:
	beq %endLocation, $t9, printEndByte.endC
	andi $t6, %byte, Cell.mask.little
	printCell ($t6)
	sll %byte, %byte, 2
	addi $t9, $t9, 1
	bne $t9, $at, printEndByte.Loop
	b printEndByte.end
	
	printEndByte.endC:
	li $a0, End.char
	syscall
	addi $t9, $t9, 1
	sll %byte, %byte, 2
	bne $t9, $at, printEndByte.Loop

	printEndByte.end:
.end_macro 

.macro printSpecialVictorybyte (%byte, %playerPos, %endLocation)
	li $at, 4 #end condition
	
	move $t9, $zero
	printSpecialByte.Loop:
	beq $t7, $t9, printSpecialByte.player
	beq %endLocation, $t9, printSpecialByte.endC
	andi $t6, %byte, Cell.mask.little
	printCell ($t6)
	sll %byte, %byte, 2
	addi $t9, $t9, 1
	bne $t9, $at, printSpecialByte.Loop
	b printSpecialByte.end
	
	printSpecialByte.player:
	li $a0, Player.char
	syscall
	sll %byte, %byte, 2
	addi $t9, $t9, 1
	bne $t9, $at, printSpecialByte.Loop
	b printSpecialByte.end

	printSpecialByte.endC:
	li $a0, EndVictory.char
	syscall
	sll %byte, %byte, 2
	addi $t9, $t9, 1
	bne $t9, $at, printSpecialByte.Loop

	printSpecialByte.end:
.end_macro 

.macro printEndVictoryByte (%byte, %endLocation)
	li $at, 4 #end condition

	move $t9, $zero
	printEndByte.Loop:
	beq %endLocation, $t9, printEndByte.endC
	andi $t6, %byte, Cell.mask.little
	printCell ($t6)
	sll %byte, %byte, 2
	addi $t9, $t9, 1
	bne $t9, $at, printEndByte.Loop
	b printEndByte.end
	
	printEndByte.endC:
	li $a0, EndVictory.char
	syscall
	addi $t9, $t9, 1
	sll %byte, %byte, 2
	bne $t9, $at, printEndByte.Loop

	printEndByte.end:
.end_macro 

.macro printVictoryLabyrinth
	li $t5, 0 #Outer Loop Index
	printVictoryLabyrinth.OuterLoop:
		addi $t4, $t5, 6 #EoL    BytesPerLine
		printVictoryLabyrinth.InnerLoop:
			add $t3, $t5, $s0	#address of byte to get
			lb $t3, ($t3)		#the byte we want to print
			bne $t5, $s5, printVictoryLabyrinth.NotPlayerByte
				bne $t5, $s3, printVictoryLabyrinth.NotPlayerByte.NotEndByte
					printSpecialVictorybyte($t3, $s6, $s4)
				b printVictoryLabyrinth.bytePrinted
				printVictoryLabyrinth.NotPlayerByte.NotEndByte:
				printPlayerByte ($t3, $s6)
				b printVictoryLabyrinth.bytePrinted
			printVictoryLabyrinth.NotPlayerByte:
			bne $t5, $s3, printVictoryLabyrinth.NotEndByte
				printEndVictoryByte ($t3, $s4)
				b printVictoryLabyrinth.bytePrinted
			printVictoryLabyrinth.NotEndByte:
			printByte ($t3)
		printVictoryLabyrinth.bytePrinted:
		addi $t5, $t5, 1
		bne $t4, $t5, printVictoryLabyrinth.InnerLoop 
		println
	li $at, TotalBytes
	bne $t5, $at, printVictoryLabyrinth.OuterLoop
.end_macro
