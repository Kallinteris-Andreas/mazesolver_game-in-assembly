.eqv findSolution.success 0
.eqv findSolution.fail 1

#this macro pushes the [$ra] and [%currentIndex] to the stack
.macro findSolution.push (%currentIndex)
	subi $sp, $sp, 8
	sw $ra, ($sp)
	sw %currentIndex, 4($sp)
.end_macro

#sets [$at] to 1 if the cell at [%index] is not a path
.macro setIfNotPath (%index)
	getCellType ($s0, %index, $at)
.end_macro

#pops the [$ra] & [%index] from the stack 
.macro findSolution.returnFail (%index)
	lw $ra, ($sp)
	lw %index, 4($sp)
	addi $sp, $sp, 8
	jr $ra
.end_macro
 
#pops the [$ra] & [%index] from the stack 
#and sets the cell at [%index] to a best_path ('#')
.macro findSolution.returnSuccess
	li $t1, Cell.best_path
	setCellType ($s0, $s7, $t1)
	lw $s7, 4($sp)

	lw $ra, 0($sp)
	addi $sp, $sp, 8
	jr $ra
.end_macro




	
