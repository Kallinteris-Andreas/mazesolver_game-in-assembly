.macro loadByteAtIndex (%base, %index, %return, %address)
	srl %address, %index, Cell.BitsPerCell	#[%address] now has the byte's relative address
	add %address, %base, %address		#[%address] now has the byte's actual address
	lb %return, (%address)			#[%return] now has the cellByte 
.end_macro



#alters $t9, $t8, $at
#Finds what type the cell at [%index] is and returns to [%return].
#[%base] is the bast address of the map
.macro getCellType (%base, %index, %return)
	loadByteAtIndex (%base, %index, $t9, $t9)	#[$t9] now has the cellByte

	andi $at, %index, Cell.mask.big #now has the 2 LSB of the index
	li $t8, 3 	#Loop index
	getCellType.loop:
		beq $t8, $at, getCellType.found
		srl $t9, $t9, Cell.BitsPerCell
		addi $t8, $t8, -1
	bne $t8, $zero, getCellType.loop

		#srl $t9, $t9, Cell.BitsPerCell

	getCellType.found:
		andi %return, $t9, Cell.mask.big
.end_macro

#alters $t9, $t8, $t7, $at, [%newType]
#Changes the cell at [%index] to [%newType].
#[%base] is the bast address of the map
.macro setCellType (%base, %index, %newType)
	loadByteAtIndex (%base, %index, $t9, $t8)	#$t9 now has the cellByte
	andi $t7, %index, Cell.mask.big			#$t7 now has the 2 LSB of the index

	#LUT
	beq $t7, $zero, setCellType.setFirstCell
	li $at, 1
	beq $t7, $at, setCellType.setSecondCell
	li $at, 2
	beq $t7, $at, setCellType.setThirdCell
	#li $at, 3
	#beq $t7, $at, setCellType.setForthCell
	setCellType.setForthCell:
		andi $t9, $t9, 0xfc
		add $t9, $t9, %newType
		b setCellType.end
	setCellType.setThirdCell:
		andi $t9, $t9, 0xf3
		sll %newType, %newType, 2
		add $t9, $t9, %newType
		b setCellType.end
	setCellType.setSecondCell:
		andi $t9, $t9, 0xcf
		sll %newType, %newType, 4
		add $t9, $t9, %newType
		b setCellType.end
	setCellType.setFirstCell:
		andi $t9, $t9, 0x3f
		sll %newType, %newType, 6
		add $t9, $t9, %newType
		#b setCellType.end
	
	setCellType.end:
		sb $t9, ($t8)			#finaly update the byte with the new value
.end_macro
