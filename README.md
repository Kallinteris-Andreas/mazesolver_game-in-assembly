# Labyrinth Minigame & Auto-Solver
This program was written as a laboratory exercise for the cource "Digital Computers" (third semester @TUC)

The program's specification includes 2 functions

1)Allow the player to move in the Labyrinth (using the "WASD" keys)

2)Find a solution in the labyrinth (using the flat fill algorithm)

### Personal Goals

Implement the provided algorithm (from the exercice's specification) to find the optimal solution as well as any other part of code with the maximum perfomance possible with a small regard to binary size

The following perfomance optimazitons have been implemented thus far

* Loop Unrolling/elminination
* Register move eliminations
* Branch & Jump Elimination
* Function Inlining


## Built With

* [Mars-Mips](https://courses.missouristate.edu/KenVollmar/MARS/) - A mips assembler and simulator which was used for development and testing

Note: Any macro assember that supports linking should work (SPIM doesn't by default)

### Build Instructions

assemble 

	main.asm
	exceptions.s

## Authors
* **Kallinteris Andreas** - *Sole contributor* - kallinteris@protonmail.com


## License

This project is licensed under the [GNU-GPL-V2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html) License (if it is ever released on the public)

## To Do
* This Project is considered complete and no modification are likely to be made by the original autor
