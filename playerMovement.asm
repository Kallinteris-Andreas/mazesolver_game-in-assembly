#,sets the printing properties of the player
.macro updatePlayerByte (%newPos, %playerByte, %relativePos)
	srl %playerByte, %newPos, 2
	andi %relativePos, %newPos, 3
.end_macro

.eqv key.move.Left 97	#'a'
.eqv key.move.Up 119	#'w'
.eqv key.move.Down 115	#'s'
.eqv key.move.Right 100	#'d'

#move the player from  [%currentPos] to new 
#set the the new pos to [%currentPos]
#Goal: try to use only $t0,$t1,$t2,$t3,$at
#
.macro movePlayer (%base, %currentByte, %currentRelative, %movement)
	sll $t1, %currentByte, 2
	add $t1, $t1, %currentRelative	#[$t1] now the the PlayerIndex

	#check if the [%movement] corrisponds to a specific input
	li $at, key.move.Right
	beq %movement, $at, movePlayer.Right
	li $at, key.move.Up
	beq %movement, $at, movePlayer.Up
	li $at, key.move.Left
	beq %movement, $at, movePlayer.Left
	li $at, key.move.Down
	beq %movement, $at, movePlayer.Down

	#if we reach here it means the wrong input was given
	b movePlayer.NoMoving

	movePlayer.Right:
		addi $t1, $t1, 1
		getCellType (%base, $t1, $t0)
		bnez $t0, movePlayer.NoMoving	#check if we are trying to move to a wall
		b movePlayer.done
	movePlayer.Up:
		subi $t1, $t1, Maze.W
		getCellType (%base, $t1, $t0)
		bnez $t0, movePlayer.NoMoving	#check if we are trying to move to a wall
		b movePlayer.done
	movePlayer.Left:
		subi $t1, $t1, 1
		getCellType (%base, $t1, $t0)
		bnez $t0, movePlayer.NoMoving	#check if we are trying to move to a wall
		b movePlayer.done
	movePlayer.Down:
		addi $t1, $t1, Maze.W
		getCellType (%base, $t1, $t0)
		bnez $t0, movePlayer.NoMoving	#check if we are trying to move to a wall
		#b movePlayer.done

	movePlayer.done:
		updatePlayerByte ($t1, %currentByte, %currentRelative)
		
		li $v0, 11
		jal printLabyrinth
		bne $t1, EndX, movePlayer.notAtExit
			li $v0, 4
			la $a0, victoryText
			syscall
			exit
		movePlayer.notAtExit:

	movePlayer.NoMoving:
.end_macro
