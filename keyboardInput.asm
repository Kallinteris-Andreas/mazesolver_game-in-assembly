## file header
#this file includes [3] methods of obtainting user input
#the prefered one should be set to readKeyboardInput


.macro readCharSYSCALL
	li $v0, 12
	syscall
	#move $v0
.end_macro

.macro readCharPolling
	li $at, 0xFFFF0000 
	readCharPolling.loop:
	lw $v1, ($at)
	msleep(20)
	beqz $v1, readCharPolling.loop
	lw $v0, 4($at)
	sw $zero, ($at)
	#exit
.end_macro

.macro readCharInterupt
	readCharInterupts.loop:
	la $at, cFlag
	lw $v1, ($at)
	#print_int($v1)
	bnez $v1, readCharInterupts.qqq

	li $at, 0xFFFF0000
	lw $v1, ($at)
	tne $v1, $zero

	msleep(20)
	#beqz $v1, readCharInterupts.
	b readCharInterupts.loop

	readCharInterupts.qqq:

	#done
	la $at, cData
	lw $v0, ($at)
	la $at, cFlag
	sw $zero, ($at)
.end_macro

#reads from keyboard and returns the char in [$v0]
.macro readKeyboardInput
	readCharSYSCALL
	#readCharPolling
	#readCharInterupt
.end_macro
