.eqv StartX 24
.eqv StartByte 6
.eqv EndX 361
.eqv EndByte 90
.eqv EndCellRelativePos 1
.eqv TotalBytes 96
.eqv BytesPerLine 6

.eqv Maze.W 24
.eqv Maze.H 16

#.eqv solutionIndex $s7

.eqv key.findSolution 101

.eqv Cell.BitsPerCell 2

.eqv Cell.path 0
.eqv Cell.wall 1
.eqv Cell.moved_path 2
.eqv Cell.best_path 3

.eqv Cell.path.little 0
.eqv Cell.wall.little 64
.eqv Cell.moved_path.little 128
.eqv Cell.best_path.little 192

.eqv Cell.mask.big 3
.eqv Cell.mask.little 192

.eqv Cell.path.char 46 #'.'
.eqv Cell.wall.char 73 #'I'
.eqv Cell.moved_path.char 42 # '*' 
.eqv Cell.best_path.char 35 # '#'

.eqv solutionInfo.success 0
.eqv solutionInfo.fail 1

.eqv Player.startByte 6
