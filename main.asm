.data
.globl map
.globl victoryText
.globl cData
.globl cFlag
.globl debug0
.globl debug1
.globl debug2
debug0: .asciiz "god kill me "
debug1: .asciiz "god kill me1 "
debug2: .asciiz "god kill me2 "

.include "utilities.asm"
.include "definitions.asm"
.include "AoSMapAccessObject.asm"
.include "solutionFinder.asm"
.include "playerMovement.asm"
.include "printer.asm"
.include "keyboardInput.asm"

cData: .align 2
	.space 4
cFlag: .align 2
	.space 4

map: .space  TotalBytes
victoryText: .asciiz "Victory!!"

.text
.globl main
main:
#init
la $s0, map

li $s1, StartX #playerPos

li $s2, EndX #
li $s3, EndByte #Const EndByte 
li $s4, EndCellRelativePos #const

li $s5, Player.startByte #playerByte
li $s6, 0 #playerRelativePos

li $s7, StartX 		#findSoultionIndex
li $v1, 1		#Solution Found operator. it becomes 0 when solution is found
li $t0, 0
li $t1, 1
li $t2, 2
li $t3, 3
li $t4, 35
.include "initMap.asm"

	jal printLabyrinth
	
#msleep(1000)
	requestInput:
		readKeyboardInput
		beq $v0, key.findSolution, trySolve
		movePlayer ($s0, $s5, $s6, $v0)
	b requestInput
	
	trySolve:
		li $v0, 11
		jal findSolution
		println
		printVictoryLabyrinth
		exit

findSolution:
	findSolution.push ($s7)

	#check if we are at the end
	bne $s7, $s2, findSolution.notExit
		li $v1, 0		#set found
		findSolution.returnSuccess
	findSolution.notExit:

	setIfNotPath ($s7)
	beq $zero, $at, findSolution.path
		jr $ra
		findSolution.returnFail ($s7)
	findSolution.path:

	msleep (2000)
	println
	mthi $ra
	jal printLabyrinth
	mfhi $ra

	li $t0, Cell.moved_path
	setCellType ($s0, $s7, $t0)

	subi $s7, $s7, Maze.W
	jal findSolution
	addi $s7, $s7, Maze.W
	bne $v1, $zero, findSolution.notDone0
		findSolution.returnSuccess
	findSolution.notDone0:

	addi $s7, $s7, Maze.W
	jal findSolution
	subi $s7, $s7, Maze.W
	bne $v1, $zero, findSolution.notDone1
		findSolution.returnSuccess
	findSolution.notDone1:


	subi $s7, $s7, 1
	jal findSolution
	addi $s7, $s7, 1
	bne $v1, $zero, findSolution.notDone2
		findSolution.returnSuccess
	findSolution.notDone2:

	addi $s7, $s7, 1
	jal findSolution
	subi $s7, $s7, 1
	bne $v1, $zero, findSolution.notDone3
		findSolution.returnSuccess
	findSolution.notDone3:

	findSolution.returnFail ($s7)
	
	##we should never here
	jr $ra

printLabyrinth:
	li $t5, 0 #Outer Loop Index
	printLabyrinth.OuterLoop:
		addi $t4, $t5, 6 #EoL    BytesPerLine
		printLabyrinth.InnerLoop:
			add $t3, $t5, $s0	#address of byte to get
			lb $t3, ($t3)		#the byte we want to print
			bne $t5, $s5, printLabyrinth.NotPlayerByte
				bne $t5, $s3, printLabyrinth.NotPlayerByte.NotEndByte
					printSpecialByte($t3, $s6, $s4)
				b printLabyrinth.bytePrinted
				printLabyrinth.NotPlayerByte.NotEndByte:
				printPlayerByte ($t3, $s6)
				b printLabyrinth.bytePrinted
			printLabyrinth.NotPlayerByte:
			bne $t5, $s3, printLabyrinth.NotEndByte
				printEndByte ($t3, $s4)
				b printLabyrinth.bytePrinted
			printLabyrinth.NotEndByte:
			printByte ($t3)
		printLabyrinth.bytePrinted:
		addi $t5, $t5, 1
		bne $t4, $t5, printLabyrinth.InnerLoop 
		println
	li $at, TotalBytes
	bne $t5, $at, printLabyrinth.OuterLoop
	jr $ra
