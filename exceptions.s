.include "utilities.asm"

# This is the exception vector address for MIPS32:
.ktext 0x80000180
	li $k0, 0xFFFF0004
	lw $k0 ($k0) 
	la $k1, cData
	sw $k0, ($k1)

	li $k0, 1
	sw $k0, 4($k1)

	mfc0 $k0,$14   # Coprocessor 0 register $14 has address of trapping instruction
	addi $k0,$k0,4 # Add 4 to point to next instruction
	mtc0 $k0,$14   # Store new address back into $14

	eret
