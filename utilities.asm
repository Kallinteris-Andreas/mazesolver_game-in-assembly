#file header
#this file includes a bunch of nicieties

.macro println
	li $v0, 11		 #Print new line
	la $a0, 10
	syscall
.end_macro

.macro print_int (%int)
	li $v0, 1		 #Print int
	move $a0, %int
	syscall
.end_macro

.macro exit
	li $v0, 10		 #exit the program
	syscall
.end_macro

.macro msleep (%duration)
	li $v0, 32		 #sleep
	li $a0, %duration
	syscall
.end_macro
